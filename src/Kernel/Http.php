<?php

namespace JyDelivery\Kernel;

use GuzzleHttp\Client;

class Http
{
  private static $headers = [];
  public static $statusCode = 0;
  
  public static function httpGet(string $url, array $query = [])
  {
    return self::request($url, 'GET', ['query' => $query]);
  }
  
  public static function httpPost(string $url, array $data = [])
  {
    return self::request($url, 'POST', ['form_params' => $data]);
  }
  
  public static function httpDelete(string $url, array $data = [])
  {
    if (strpos($url, '?') === false) {
      $query = '?' . http_build_query($data);
    } else {
      $query = '&' . http_build_query($data);
    }
    return self::request($url . $query, 'DELETE');
  }
  
  public static function httpPatch(string $url, array $data = [])
  {
    if (strpos($url, '?') === false) {
      $query = '?' . http_build_query($data);
    } else {
      $query = '&' . http_build_query($data);
    }
    return self::request($url, 'PATCH', ['body' => http_build_query($data)]);
  }
  
  public static function httpPostJson(string $url, array $data = [], $query = null, $returnJson = true)
  {
    return self::request($url, 'POST', ['query' => $query, 'json' => $data], $returnJson);
  }
  
  public static function httpUpload(string $url, array $files = [], array $form = [], array $query = [])
  {
    $multipart = [];
    
    foreach ($files as $name => $path) {
      $filename    = pathinfo($path, PATHINFO_BASENAME);
      $multipart[] = [
        'name'     => $name,
        'contents' => file_exists($path) ? fopen($path, 'r') : '',
        'headers'  => [
          'Content-Disposition' => 'form-data; name="' . $name . '"; filename="' . $filename . '"',
        ],
      ];
    }
    foreach ($form as $name => $contents) {
      $multipart[] = compact('name', 'contents');
    }
    
    return self::request(
      $url,
      'POST',
      [
        'query'           => $query,
        'multipart'       => $multipart,
        'connect_timeout' => 60,
        'timeout'         => 60,
        'read_timeout'    => 60,
      ]
    );
  }
  
  public static function request($url, $method = 'GET', array $options = [], $returnJson = true)
  {
    $options['http_errors'] = false;
    
    // 协议头
    if ($options['headers']) {
      $options['headers'] = array_merge(self::$headers, $options['headers']);
    } elseif (self::$headers) {
      $options['headers'] = self::$headers;
    }
    
    $client = new Client();
    return self::handleResponse($client->request($method, $url, $options), $returnJson);
  }
  
  /**
   * 处理响应内容
   *
   * @param $request
   * @param $returnJson
   * @return mixed
   */
  public static function handleResponse($request, $returnJson)
  {
    $res              = $request->getBody()->getContents();
    self::$statusCode = $request->getStatusCode();
    self::setHeaders([]);
    if ($returnJson && !is_null($temp = json_decode($res, true))) {
      return $temp;
    }
    return $res;
  }
  
  public static function setHeaders($headers)
  {
    self::$headers = $headers;
  }
}
