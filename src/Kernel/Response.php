<?php

namespace JyDelivery\Kernel;

class Response
{
    protected $error;
    protected $platformName = '';
    protected $errorNum = [];
    
    public function fail($value, $status = 0)
    {
        exit(json_encode([
            'message' => $value,
            'status'  => $status,
        ]));
    }
    
    protected function setError($error)
    {
        if (isset($error['code']) && isset($this->errorNum[$error['code']])) {
            $error['msg'] = $this->errorNum[$error['code']];
        } else if (is_string($error)) {
            $error = ['msg' => $error];
        }
        $error['msg'] = $this->platformName . ':' . $error['msg'];
        $this->error  = $error;
        return false;
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * 生成uuid
     *
     * @param string $separate 分隔符
     * @return string
     */
    protected static function uuid($separate = '-')
    {
        $chars = md5(uniqid(mt_rand(), true));
        return substr($chars, 0, 8) . $separate
               . substr($chars, 8, 4) . $separate
               . substr($chars, 12, 4) . $separate
               . substr($chars, 16, 4) . $separate
               . substr($chars, 20, 12);
    }
    
    /**
     * 从数组中筛选特定字段(只获取特定的字段)
     *
     * @param array $data   源数组
     * @param mixed $fields 筛选字段
     * @param int   $level  处理级别
     * @return array
     */
    protected function onlyfields(&$data, $fields, $level = 1)
    {
        if ($level > 2) {
            return $data;
        }
        if ($fields == '*') {
            return $data;
        }
        if (!is_array($data)) {
            return $data;
        }
        if (is_string($fields)) {
            $fields = explode(',', $fields);
        }
        if (!is_array($fields)) {
            $fields = (array)$fields;
        }
        
        if (!is_assoc($data)) {
            foreach ($data as &$v) {
                onlyfields($v, $fields, $level + 1);
            }
            return $data;
        }
        
        foreach (array_keys($data) as $k) {
            $flag = false;
            foreach ($fields as $f) {
                if (fnmatch($f, $k)) {
                    $flag = true;
                    break;
                }
            }
            
            if (!$flag) {
                unset($data[$k]);
            }
        }
        return $data;
    }
    
    /**
     * 高德坐标 转换 百度坐标
     *
     * @param float $lat
     * @param float $lng
     * @return array
     */
    protected function amapToBaidu(&$lat, &$lng)
    {
        $x_pi        = 3.14159265358979324 * 3000.0 / 180.0;
        $x           = $lng;
        $y           = $lat;
        $z           = sqrt($x * $x + $y * $y) - 0.00002 * sin($y * $x_pi);
        $theta       = atan2($y, $x) - 0.000003 * cos($x * $x_pi);
        $data['lat'] = $lat = $z * sin($theta) + 0.006;
        $data['lng'] = $lng = $z * cos($theta) + 0.0065;
        return $data;
    }
    
    protected function random($length = 16)
    {
        $string = '';
        while (($len = strlen($string)) < $length) {
            $size   = $length - $len;
            $bytes  = random_bytes($size);
            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }
        return $string;
    }
}
