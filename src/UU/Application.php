<?php

namespace JyDelivery\UU;

use JyDelivery\Kernel\Response;

/**
 * 闪送
 */
class Application extends Response
{
    use Shop;
    use Order;
    use User;
    use Other;
    
    const HOST     = 'https://openapi.uupt.com';
    const HOST_DEV = 'http://openapi.test.uupt.com';
    private $config;
    protected $platformName = 'UU';
    
    public function __construct(array $config = [])
    {
        if (isset($config['sandbox']) && $config['sandbox'] && isset($config['sandbox_key']) && $config['sandbox_key']) {
            $config['key']     = $config['sandbox_key'];
            $config['secret']  = $config['sandbox_secret'];
            $config['open_id'] = $config['sandbox_open_id'];
        }
        if (empty($config['key']) || empty($config['secret'])) {
            exit('key或secret不能为空');
        }
        $this->config = $config;
        $this->setError('');
    }
    
    private function getHost()
    {
        return $this->config['sandbox'] ? self::HOST_DEV : self::HOST;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($data = [], $not_openid = false)
    {
        $params = array_merge([
            'appid'     => $this->config['key'],
            'openid'    => $this->config['open_id'],
            'nonce_str' => $this->random(32),
            'timestamp' => (string)time(),
        ], $data);
        
        if ($not_openid) {
            unset($params['openid']);
        }
        $params['sign'] = $this->getSign($params);
        return $params;
    }
    
    /**
     * 生成签名
     */
    private function getSign($data)
    {
        ksort($data);
        $str = [];
        foreach ($data as $key => $value) {
            if ($value === '') {
                continue;
            }
            if (!is_null($value)) {
                $str[] = $key . '=' . $value;
            }
        }
        $str[] = 'key=' . $this->config['secret'];
        $str   = mb_strtoupper(implode('&', $str));
        return strtoupper(md5($str));
    }
    
    /**
     * 获取错误代码
     *
     * @param $key
     * @return string
     */
    private function getCodeMap($key)
    {
        $codeMap = [
            '-101'   => '参数格式校验错误',
            '-102'   => 'timestamp错误',
            '-103'   => 'appid无效',
            '-104'   => '签名校验失败',
            '-105'   => 'openid无效',
            '-199'   => '参数格式校验错误',
            '-1001'  => '无法解析起始地',
            '-1002'  => '无法解析目的地',
            '-1003'  => '无法获取订单城市相关信息',
            '-1004'  => '订单小类出现错误',
            '-1005'  => '没有用户信息',
            '-1006'  => '优惠券ID错误',
            '-2001'  => 'price_token无效',
            '-2002'  => 'price_token无效',
            '-2003'  => '收货人电话格式错误',
            '-2004'  => 'special_type错误',
            '-2005'  => 'callme_withtake错误',
            '-2006'  => 'order_price错误',
            '-2007'  => 'balance_paymoney错误',
            '-2008'  => '订单总金额错误',
            '-2009'  => '支付金额错误',
            '-2010'  => '用户不一致',
            '-2011'  => '手机号错误',
            '-2012'  => '不存在绑定关系',
            '-4001'  => '取消原因不能为空',
            '-4002'  => '订单编号无效',
            '-5001'  => '订单编号无效',
            '-5002'  => '订单编号无效',
            '-5003'  => '订单编号无效',
            '-10001' => '发送频率过快，请稍候重试',
            '-11001' => '请输入正确的验证码',
        ];
        return isset($codeMap[$key]) ? $codeMap[$key] : $key;
    }
}
