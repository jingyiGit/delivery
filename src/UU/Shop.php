<?php

namespace JyDelivery\UU;

use JyDelivery\Kernel\Http;

/**
 * 店铺
 */
trait Shop
{
    /**
     * 获取门店列表
     * http://open.uupt.com/#/PlatformServe/InterfaceList/getshoplist?PlatType=2&t=%E8%8E%B7%E5%8F%96%E9%97%A8%E5%BA%97%E5%88%97%E8%A1%A8
     *
     * @param int $page
     * @return mixed
     */
    public function getShopList($page = 1)
    {
        $params = $this->getCommonParam(['pageindex' => $page]);
        $res    = Http::httpPost($this->getHost() . '/v2_0/getshoplist.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res['list'];
        }
        return $this->setError($res);
    }
}
