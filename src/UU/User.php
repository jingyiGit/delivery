<?php

namespace JyDelivery\UU;

use JyDelivery\Kernel\Http;

/**
 * 用户
 */
trait User
{
    /**
     * 发送短信验证码
     * http://open.uupt.com/#/PlatformServe/InterfaceList/binduserapply?PlatType=2&t=%E5%8F%91%E9%80%81%E7%9F%AD%E4%BF%A1%E9%AA%8C%E8%AF%81%E7%A0%81
     *
     * @param array $param
     * @return mixed
     */
    public function bindUserApply($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/binduserapply.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 获取用户openid
     * http://open.uupt.com/#/PlatformServe/InterfaceList/bindusersubmit?PlatType=2&t=%E8%8E%B7%E5%8F%96%E7%94%A8%E6%88%B7openid
     *
     * @param array $param
     * @return mixed
     */
    public function bindUserSubmit($param)
    {
        $params = $this->getCommonParam($param, true);
        $res    = Http::httpPost($this->getHost() . '/v2_0/bindusersubmit.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 用户解除绑定
     * http://open.uupt.com/#/PlatformServe/InterfaceList/cancelbind?PlatType=2&t=%E7%94%A8%E6%88%B7%E8%A7%A3%E9%99%A4%E7%BB%91%E5%AE%9A
     *
     * @param string $openid
     * @return mixed
     */
    public function cancelBind($openid = '')
    {
        $param = [];
        if ($openid) {
            $param['openid'] = $openid;
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/cancelbind.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 获取充值地址
     * http://open.uupt.com/#/PlatformServe/InterfaceList/getrecharge?PlatType=2&t=%E8%8E%B7%E5%8F%96%E5%85%85%E5%80%BC%E5%9C%B0%E5%9D%80
     *
     * @return mixed
     */
    public function getRechargeUrl()
    {
        $params = $this->getCommonParam([]);
        $res    = Http::httpPost($this->getHost() . '/v2_0/getrecharge.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res['RechargeURL'];
        }
        return $this->setError($res);
    }
    
    /**
     * 获取余额详情
     * http://open.uupt.com/#/PlatformServe/InterfaceList/getbalancedetail?PlatType=2&t=%E8%8E%B7%E5%8F%96%E4%BD%99%E9%A2%9D%E8%AF%A6%E6%83%85
     *
     * @return mixed
     */
    public function getUserAccount()
    {
        $params = $this->getCommonParam([]);
        $res    = Http::httpPost($this->getHost() . '/v2_0/getbalancedetail.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
}
