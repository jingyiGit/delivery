<?php

namespace JyDelivery\UU;

use JyDelivery\Kernel\Http;

/**
 * 其他
 */
trait Other
{
    /**
     * 查询开通城市
     * http://open.uupt.com/#/PlatformServe/InterfaceList/citylist?PlatType=2&t=%E5%B7%B2%E5%BC%80%E9%80%9A%E5%9F%8E%E5%B8%82%E5%8C%BA%E5%8E%BF%E5%88%97%E8%A1%A8
     *
     * @return mixed
     */
    public function openCitiesLists()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpPost($this->getHost() . '/v2_0/citylist.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res['citylist'];
        }
        return $this->setError($res);
    }
}
