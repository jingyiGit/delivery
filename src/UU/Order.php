<?php

namespace JyDelivery\UU;

use JyDelivery\Kernel\Http;

/**
 * 订单
 */
trait Order
{
    /**
     * 计算跑腿费
     * http://open.uupt.com/#/PlatformServe/InterfaceList/getorderprice?PlatType=2&t=%E8%AE%A1%E7%AE%97%E8%AE%A2%E5%8D%95%E4%BB%B7%E6%A0%BC
     *
     * @param array $param 参数
     * @return bool
     */
    public function calculateFee($param)
    {
        // 将高德坐标转成百度的，闪送只认百度的
        if ($param['from_lat'] && $param['from_lng']) {
            $this->amapToBaidu($param['from_lat'], $param['from_lng']);
        }
        
        // 将高德坐标转成百度的，闪送只认百度的
        if ($param['to_lat'] && $param['to_lng']) {
            $this->amapToBaidu($param['to_lat'], $param['to_lng']);
        }
        
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/getorderprice.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 提交订单
     * http://open.uupt.com/#/PlatformServe/InterfaceList/addorder?PlatType=2&t=%E5%8F%91%E5%B8%83%E8%AE%A2%E5%8D%95
     *
     * @param array $param 参数
     * @return bool
     */
    public function orderPlace($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/addorder.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 取消订单
     * http://open.uupt.com/#/PlatformServe/InterfaceList/cancelorder?PlatType=2&t=%E5%8F%96%E6%B6%88%E8%AE%A2%E5%8D%95
     *
     * @param array $param 参数
     * @return bool
     */
    public function cancelOrder($param)
    {
        if (isset($param['order_id'])) {
            $param['order_code'] = $param['order_id'];
            unset($param['order_id']);
        }
        if (isset($param['third_order_id'])) {
            $param['origin_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        if (isset($param['cancel_reason'])) {
            $param['reason'] = $param['cancel_reason'];
            unset($param['cancel_reason']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/cancelorder.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 查询订单详情
     * http://open.uupt.com/#/PlatformServe/InterfaceList/getorderdetail?PlatType=2&t=%E8%8E%B7%E5%8F%96%E8%AE%A2%E5%8D%95%E8%AF%A6%E6%83%85
     *
     * @param array $param 参数
     * @return bool
     */
    public function getOrderInfo($param)
    {
        if (isset($param['order_id'])) {
            $param['order_code'] = $param['order_id'];
            unset($param['order_id']);
        }
        if (isset($param['third_order_id'])) {
            $param['origin_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/getorderdetail.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 支付小费
     * http://open.uupt.com/#/PlatformServe/InterfaceList/payonlinefee?PlatType=2&t=%E6%94%AF%E4%BB%98%E5%B0%8F%E8%B4%B9
     *
     * @param array $param 参数
     * @return bool
     */
    public function addTip($param)
    {
        if (isset($param['order_id'])) {
            $param['order_code'] = $param['order_id'];
            unset($param['order_id']);
        }
        if (isset($param['third_order_id'])) {
            $param['origin_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/payonlinefee.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 获取违约金
     * http://open.uupt.com/#/PlatformServe/InterfaceList/gethomeservicefee?PlatType=2&t=%E8%8E%B7%E5%BE%97%E8%BF%9D%E7%BA%A6%E9%87%91
     *
     * @param array $param 参数
     * @return bool
     */
    public function getHomeServiceFee($param)
    {
        if (isset($param['order_id'])) {
            $param['order_code'] = $param['order_id'];
            unset($param['order_id']);
        }
        if (isset($param['third_order_id'])) {
            $param['origin_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/gethomeservicefee.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 获取计价物品重量配置信息
     * http://open.uupt.com/#/PlatformServe/InterfaceList/getgoodsweigthlist?PlatType=2&t=%E8%8E%B7%E5%8F%96%E8%AE%A1%E4%BB%B7%E7%89%A9%E5%93%81%E9%87%8D%E9%87%8F%E9%85%8D%E7%BD%AE%E4%BF%A1%E6%81%AF
     *
     * @param array $param 参数
     * @return bool
     */
    public function getGoodsWeigthList($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/v2_0/getgoodsweigthlist.ashx', $params);
        if ($res['return_code'] == 'ok') {
            return $res['List'];
        }
        return $this->setError($res);
    }
}
