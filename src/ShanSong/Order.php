<?php

namespace JyDelivery\ShanSong;

use JyDelivery\Kernel\Http;

/**
 * 店铺
 */
trait Order
{
    /**
     * 计算跑腿费
     * https://open.ishansong.com/documentCenter/305
     *
     * @param array $param 参数
     * @return bool
     */
    public function calculateFee($param)
    {
        // 将高德坐标转成百度的，闪送只认百度的
        if ($param['sender']['fromLatitude'] && $param['sender']['fromLongitude']) {
            $this->amapToBaidu($param['sender']['fromLatitude'], $param['sender']['fromLongitude']);
        }
        
        // 将高德坐标转成百度的，闪送只认百度的
        if ($param['receiverList']['toLatitude'] && $param['receiverList']['toLongitude']) {
            $this->amapToBaidu($param['receiverList']['toLatitude'], $param['receiverList']['toLongitude']);
        }
        
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/orderCalculate', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        return $this->setError($res);
    }
    
    /**
     * 提交订单
     * https://open.ishansong.com/documentCenter/306
     *
     * @param array $param 参数
     * @return void
     */
    public function orderPlace($param)
    {
        if (isset($param['order_id'])) {
            $param['issOrderNo'] = $param['order_id'];
            unset($param['order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/orderPlace', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        return $this->setError($res);
    }
    
    /**
     * 订单加价
     * https://open.ishansong.com/documentCenter/307
     *
     * @param array $param 参数
     * @return bool
     */
    public function addTip($param)
    {
        if (isset($param['order_id'])) {
            $param['issOrderNo'] = $param['order_id'];
            unset($param['order_id']);
        }
        if (isset($param['tips'])) {
            $param['additionAmount'] = $param['tips'];
            unset($param['tips']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/addition', $params);
        if ($res['status'] == 200) {
            return true;
        }
        return $this->setError($res);
    }
    
    /**
     * 查询订单详情
     * https://open.ishansong.com/documentCenter/308
     *
     * @param array $param 参数
     * @return bool
     */
    public function getOrderInfo($param)
    {
        if (isset($param['order_id'])) {
            $param['issOrderNo'] = $param['order_id'];
            unset($param['order_id']);
        }
        if (isset($param['third_order_id'])) {
            $param['thirdOrderNo'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/orderInfo', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        return $this->setError($res);
    }
    
    /**
     * 订单预取消
     * https://open.ishansong.com/documentCenter/312
     *
     * @param array $param 参数
     * @return bool
     */
    public function preCancelOrder($param)
    {
        if (isset($param['order_id'])) {
            $param['issOrderNo'] = $param['order_id'];
            unset($param['order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/preAbortOrder', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        return $this->setError($res);
    }
    
    /**
     * 订单取消
     * https://open.ishansong.com/documentCenter/313
     *
     * @param array $param 参数
     * @return bool
     */
    public function cancelOrder($param)
    {
        if (isset($param['order_id'])) {
            $param['issOrderNo'] = $param['order_id'];
            unset($param['order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/abortOrder', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        return $this->setError($res);
    }
}
