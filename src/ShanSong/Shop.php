<?php

namespace JyDelivery\ShanSong;

use JyDelivery\Kernel\Http;

/**
 * 店铺
 */
trait Shop
{
    /**
     * 查询商户店铺
     * https://open.ishansong.com/documentCenter/302
     *
     * @return mixed
     */
    public function shopInfo()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/queryAllStores', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
}
