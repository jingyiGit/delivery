<?php

namespace JyDelivery\ShanSong;

use JyDelivery\Kernel\Http;

/**
 * 授权
 * https://open.ishansong.com/documentCenter/327
 */
trait Auth
{
    /**
     * 获取授权Url
     *
     * @param string $shop_id      第三方店铺ID
     * @param string $redirect_uri 授权成功后的跳转地址
     * @return string
     */
    public function getAuthUrl($shop_id, $redirect_uri)
    {
        $param = [
            'response_type' => 'code',
            'client_id'     => $this->config['key'],
            'state'         => $shop_id,
            'scope'         => 'shop_open_api',
            'thirdStoreId'  => '0',
            'redirect_uri'  => $redirect_uri,
        ];
        return $this->getHost() . "/auth?" . http_build_query($param);
    }
    
    /**
     * 获取token
     *
     * @param string $code 授权跳转回来的 Code
     * @return mixed
     */
    public function getAccessToken($code)
    {
        $params = [
            'clientId' => $this->config['key'],
            'code'     => $code,
        ];
        $res    = Http::httpPost($this->getHost() . '/openapi/oauth/token', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 刷新AccessToken
     *
     * @param string $refreshToken 通过 getAccessToken 获得
     * @return void
     */
    public function refreshToken($refreshToken)
    {
        $params = $this->getCommonParam([
            'refreshToken' => $refreshToken,
        ]);
        $res    = Http::httpPost($this->getHost() . '/openapi/oauth/refresh_token', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取消授权
     *
     * @return bool
     */
    public function cancelAuth()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpPost($this->getHost() . '/openapi/oauth/cancel', $params);
        if ($res['status'] == 200) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 查询账号余额
     * https://open.ishansong.com/documentCenter/357
     *
     * @return bool
     */
    public function getUserAccount()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/getUserAccount', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
}
