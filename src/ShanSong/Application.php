<?php

namespace JyDelivery\ShanSong;

use JyDelivery\Kernel\Response;

/**
 * 闪送
 */
class Application extends Response
{
    use Auth;
    use Shop;
    use Order;
    use Other;
    
    const HOST     = 'https://open.ishansong.com';
    const HOST_DEV = 'http://open.s.bingex.com';
    private $config;
    protected $platformName = 'ShanSong';
    protected $accessToken = '';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    public function setAccessToken($access_token)
    {
        $this->accessToken = $access_token;
    }
    
    private function getHost()
    {
        return $this->config['sandbox'] ? self::HOST_DEV : self::HOST;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($data = [])
    {
        $params         = [
            'clientId'    => $this->config['key'],
            'accessToken' => $this->accessToken,
            'timestamp'   => time() . rand(100, 999),
            'data'        => json_encode($data, JSON_UNESCAPED_UNICODE),
        ];
        $params['sign'] = $this->getSign($params);
        return $params;
    }
    
    /**
     * 生成签名
     */
    private function getSign($data)
    {
        ksort($data);
        $str = $this->config['secret'];
        foreach ($data as $key => $value) {
            if (!$value || $key == 'file') {
                continue;
            }
            $str .= $key . $value;
        }
        return strtoupper(md5($str));
    }
}
