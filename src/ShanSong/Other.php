<?php

namespace JyDelivery\ShanSong;

use JyDelivery\Kernel\Http;

/**
 * 其他
 */
trait Other
{
    /**
     * 查询开通城市
     * https://open.ishansong.com/documentCenter/301
     *
     * @return mixed
     */
    public function openCitiesLists()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/openCitiesLists', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 查询城市可指定交通方式
     * https://open.ishansong.com/documentCenter/304
     *
     * @param int $cityId 城市ID，查询开通城市接口获取，对应id字段
     * @return mixed
     */
    public function optionalTravelWay($cityId)
    {
        $params = $this->getCommonParam([
            'cityId' => intval($cityId),
        ]);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/optionalTravelWay', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取骑手信息
     * https://open.ishansong.com/documentCenter/309
     *
     * @param array $param 参数
     * @return bool
     */
    public function getRiderInfo($param)
    {
        if (isset($param['order_id'])) {
            $param['issOrderNo'] = $param['order_id'];
            unset($param['order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->getHost() . '/openapi/developer/v5/courierInfo', $params);
        if ($res['status'] == 200) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
}
