<?php

namespace JyDelivery\Dada;

use JyDelivery\Kernel\Http;

/**
 * 店铺
 */
trait Shop
{
    /**
     * 注册商户
     * http://newopen.imdada.cn/#/development/file/merchantAdd
     *
     * @param array $param 参数
     * @return bool
     */
    public function merchantCreate($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/merchantApi/merchant/add', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 查询商户余额
     * http://newopen.imdada.cn/#/development/file/balanceQuery
     *
     * @param int $category 查询运费账户类型（1：运费账户；2：红包账户，3：所有），默认查询运费账户余额
     * @return bool
     */
    public function merchantAccount($category = 1)
    {
        $params = $this->getCommonParam(['category' => $category]);
        $res    = Http::httpPostJson($this->getHost() . '/api/balance/query', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取充值链接
     * http://newopen.imdada.cn/#/development/file/recharge
     *
     * @param array $param
     * @return bool
     */
    public function merchantRecharge($param)
    {
        if (!isset($param['category'])) {
            $param['category'] = 'H5';
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/recharge', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 新增门店
     * http://newopen.imdada.cn/#/development/file/shopAdd
     *
     * @param array $param 参数
     * @return bool
     */
    public function shopAdd($param)
    {
        $params = $this->getCommonParam([$param]);
        $res    = Http::httpPostJson($this->getHost() . '/api/shop/add', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 编辑门店
     * http://newopen.imdada.cn/#/development/file/shopUpdate
     *
     * @param array $param 参数
     * @return bool
     */
    public function shopUpdate($param)
    {
        if (!isset($param['origin_shop_id'])) {
            return $this->setError('更新门店信息失败，门店编码不能为空');
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/shop/update', $params);
        if ($res['status'] == 'success') {
            return true;
        }
        return $this->setError($res);
    }
    
    /**
     * 查询门店信息
     * http://newopen.imdada.cn/#/development/file/shopDetail
     *
     * @param string $origin_shop_id 门店编码
     * @return bool
     */
    public function shopInfo($origin_shop_id)
    {
        $params = $this->getCommonParam(['origin_shop_id' => $origin_shop_id]);
        $res    = Http::httpPostJson($this->getHost() . '/api/shop/detail', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
}
