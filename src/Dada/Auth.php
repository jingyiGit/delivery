<?php

namespace JyDelivery\Dada;

use JyDelivery\Kernel\Http;

/**
 * 授权
 */
trait Auth
{
    /**
     * getTicket
     *
     * @return string
     */
    private function getTicket()
    {
        $param         = [
            'appKey' => $this->config['key'],
            'nonce'  => $this->random(15),
        ];
        $param['sign'] = $this->getSignBySha1($param);
        $res           = Http::httpGet($this->getHost() . "/third/party/ticket", $param);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 获取授权Url
     *
     * @param string $shop_id      第三方店铺ID
     * @param string $state        回调标识
     * @param string $redirect_uri 授权成功后的跳转地址
     * @param int    $resultType   是否跳转（0-默认结果页， 1-跳转）
     * @return string
     */
    public function getAuthUrl($shop_id, $state, $redirect_uri, $resultType = 1)
    {
        $param                = [
            'appKey' => $this->config['key'],
            'shopId' => $shop_id,
            'nonce'  => $this->random(15),
            'ticket' => $this->getTicket(),
        ];
        $param['sign']        = $this->getSignBySha1($param);
        $param['state']       = $state;           // 回调标识
        $param['resultType']  = $resultType;      // 是否跳转（0-默认结果页， 1-跳转）
        $param['redirectUrl'] = $redirect_uri;
        return $this->getHost() . "/third/party/oauth?" . http_build_query($param);
    }
    
    /**
     * 取授权信息
     *
     * @param string $ticket getAuthUrl 授权成功后，跳转的url里带的
     * @return false|mixed
     */
    public function getAuthInfo($ticket)
    {
        $param = [
            'ticket' => $ticket,
        ];
        $res   = Http::httpGet($this->getHost() . "/third/party/auth/info", $param);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        return $this->setError($res);
    }
}
