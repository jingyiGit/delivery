<?php

namespace JyDelivery\Dada;

use JyDelivery\Kernel\Http;

/**
 * 其他
 */
trait Other
{
    /**
     * 查询开通城市
     * http://newopen.imdada.cn/#/development/file/cityList
     *
     * @return mixed
     */
    public function openCitiesLists()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpPostJson($this->getHost() . '/api/cityCode/list', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 取骑手信息
     * http://newopen.imdada.cn/#/development/file/listTransportersToAppoint
     *
     * @param array $shop_no 门店编码
     * @return bool
     */
    public function getRiderInfo($shop_no)
    {
        $params = $this->getCommonParam(['shop_no' => $shop_no]);
        $res    = Http::httpPost($this->getHost() . '/api/order/appoint/list/transporter', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        return $this->setError($res);
    }
}
