<?php

namespace JyDelivery\Dada;

use JyDelivery\Kernel\Response;

/**
 * 达达
 */
class Application extends Response
{
    use Auth;
    use Shop;
    use Order;
    use Other;
    
    const HOST     = 'https://newopen.imdada.cn';
    const HOST_DEV = 'http://newopen.qa.imdada.cn';
    private $config;
    protected $platformName = 'Dada';
    private $source_id;         // 达达商户ID
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    public function setSourceId($source_id)
    {
        $this->source_id = $source_id;
    }
    
    private function getHost()
    {
        return $this->config['sandbox'] ? self::HOST_DEV : self::HOST;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($data = [])
    {
        $params              = [
            'app_key'   => $this->config['key'],
            'timestamp' => time() . rand(100, 999),
            'format'    => 'json',
            'v'         => '1.0',
            'source_id' => $this->source_id,
            'body'      => json_encode($data, JSON_UNESCAPED_UNICODE),
        ];
        $params['signature'] = $this->getSign($params);
        return $params;
    }
    
    /**
     * 生成签名
     */
    private function getSign($data)
    {
        ksort($data);
        $args = "";
        foreach ($data as $key => $value) {
            $args .= $key . $value;
        }
        $args = $this->config['secret'] . $args . $this->config['secret'];
        return strtoupper(md5($args));
    }
    
    private function getSignBySha1($data)
    {
        $data['appSecret'] = $this->config['secret'];
        $data              = array_values($data);
        sort($data);
        $args = implode('', $data);
        return sha1($args);
    }
}
