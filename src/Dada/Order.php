<?php

namespace JyDelivery\Dada;

use JyDelivery\Kernel\Http;

/**
 * 店铺
 */
trait Order
{
    /**
     * 计算跑腿费
     * http://newopen.imdada.cn/#/development/file/readyAdd
     *
     * @param array $param 参数
     * @return bool
     */
    public function calculateFee($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/order/queryDeliverFee', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 提交订单
     * http://newopen.imdada.cn/#/development/file/readyAdd
     *
     * @param array $param 参数
     * @return bool
     */
    public function orderPlace($param)
    {
        if (isset($param['order_id'])) {
            $param['deliveryNo'] = $param['order_id'];
            unset($param['order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/order/addAfterQuery', $params);
        if ($res['status'] == 'success') {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 订单加价
     * http://newopen.imdada.cn/#/development/file/addTip
     *
     * @param array $param 参数
     * @return bool
     */
    public function addTip($param)
    {
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/order/addTip', $params);
        if ($res['status'] == 'success') {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 查询订单详情
     * http://newopen.imdada.cn/#/development/file/statusQuery
     *
     * @param array $param 参数
     * @return bool
     */
    public function getOrderInfo($param)
    {
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/order/status/query', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 订单取消
     * http://newopen.imdada.cn/#/development/file/formalCancel
     *
     * @param array $param 参数
     * @return bool
     */
    public function cancelOrder($param)
    {
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        if (!isset($param['cancel_reason_id'])) {
            $param['cancel_reason_id'] = 10000;
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/order/formalCancel', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 妥投异常之物品返回完成
     * http://newopen.imdada.cn/#/development/file/abnormalConfirm
     *
     * @param array $param 参数
     * @return bool
     */
    public function goods($param)
    {
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/api/order/confirm/goods', $params);
        if ($res['status'] == 'success') {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取取消原因列表
     * http://newopen.imdada.cn/#/development/file/reasonList
     *
     * @return void
     */
    public function getReasons()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpPostJson($this->getHost() . '/api/order/cancel/reasons', $params);
        if ($res['status'] == 'success') {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
}
