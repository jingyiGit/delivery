<?php

namespace JyDelivery\Sf;

use JyDelivery\Kernel\Http;

/**
 * 其他
 */
trait Other
{
    /**
     * 取骑手信息
     *
     * @param array $param 参数
     * @return bool|array|string
     */
    public function getRiderInfo($param)
    {
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/riderlatestposition' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'] ?: ['shop_order_id' => $param['order_id']];
        }
        return $this->setError($res);
    }
    
    /**
     * 获取商户信息
     *
     * @param array $param 参数 [shop_id, shop_type]
     * @return bool|array|string
     */
    public function getshopinfo($param)
    {
        // 店铺ID类型	1:顺丰店铺ID 2:接⼊⽅店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/getshopinfo' . $this->getSign($params), $params);
        dd($res);
        if ($res['error_code'] == 0) {
            return $res['result'] ?: ['shop_order_id' => $param['order_id']];
        }
        return $this->setError($res);
    }
    
    /**
     * 取配送员轨迹H5
     *
     * @param array $param 参数
     * @return bool
     */
    public function getRiderH5($param)
    {
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/riderviewv2' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
}
