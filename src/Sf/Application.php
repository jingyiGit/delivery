<?php

namespace JyDelivery\Sf;

use JyDelivery\Kernel\Response;

/**
 * 顺丰
 * https://openic.sf-express.com/#/apidoc
 */
class Application extends Response
{
    use Auth;
    use Order;
    use Other;
    
    const HOST = 'https://openic.sf-express.com';
    private $config;
    protected $platformName = 'Sf';
    private $shop_id;               // 店铺ID
    private $shop_type;             // 1:顺丰店铺ID 2:接入方店铺ID
    protected $accessToken = '';
    
    public function __construct(array $config = [])
    {
        if (isset($config['sandbox']) && $config['sandbox'] && isset($config['sandbox_key']) && $config['sandbox_key']) {
            $config['key']    = $config['sandbox_key'];
            $config['secret'] = $config['sandbox_secret'];
        }
        if (empty($config['key']) || empty($config['secret'])) {
            exit('key或secret不能为空');
        }
        
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 设置店铺信息
     *
     * @param int    $shop_id   店铺ID
     * @param string $shop_type 1:顺丰店铺ID 2:接入方店铺ID
     * @return void
     */
    public function setShopInfo($shop_id, $shop_type)
    {
        $this->shop_id   = $shop_id;
        $this->shop_type = $shop_type;
    }
    
    public function setAccessToken($access_token)
    {
        $this->accessToken = $access_token;
    }
    
    private function getHost()
    {
        return self::HOST;
    }
    
    /**
     * 取公共参数
     *
     * @param bool $enterprise 是否为企业
     */
    private function getCommonParam($data = [], $enterprise = false)
    {
        $data = array_merge([
            'dev_id'    => $this->config['key'],
            'push_time' => time(),
        ], $data);
        // if (!$enterprise) {
        //     $data['shop_id']   = $this->shop_id;
        //     $data['shop_type'] = $this->shop_type;
        // }
        return $data;
    }
    
    /**
     * 生成签名
     */
    private function getSign($data)
    {
        $post_data = json_encode($data);
        $sign_char = $post_data . "&{$this->config['key']}&{$this->config['secret']}";
        return '?sign=' . base64_encode(strtolower(md5($sign_char)));
    }
}
