<?php

namespace JyDelivery\Sf;

use JyDelivery\Kernel\Http;

/**
 * 店铺
 */
trait Order
{
    /**
     * 计算跑腿费
     *
     * @param array $param 参数
     * @return bool
     */
    public function calculateFee($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/precreateorder' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 提交订单
     *
     * @param array $param 参数
     * @return bool
     */
    public function orderPlace($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/createorder' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 订单加价
     *
     * @param array $param 参数
     * @return bool
     */
    public function addTip($param)
    {
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        
        // 订单小费，单位分，加小费最低不能少于100分
        if (isset($param['tips'])) {
            $param['gratuity_fee'] = $param['tips'];
            unset($param['tips']);
        }
        
        // 统一字段
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/addordergratuityfee' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 查询订单加小费信息
     *
     * @param array $param 参数
     * @return bool
     */
    public function getTipsInfo($param)
    {
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/getordergratuityfee' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 查询订单详情
     *
     * @param array $param 参数
     * @return bool
     */
    public function getOrderInfo($param)
    {
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/getorderstatus' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 查询订单状态流
     *
     * @param array $param 参数
     * @return bool
     */
    public function getOrderStatus($param)
    {
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/listorderfeed' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 预取消订单
     *
     * @param array $param 参数
     * @return bool
     */
    public function preCancelOrder($param)
    {
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/precancelorder' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
    
    /**
     * 订单取消
     *
     * @param array $param 参数
     * @return bool
     */
    public function cancelOrder($param)
    {
        // 1、顺丰订单号 2、商家订单号
        if (!isset($param['order_type'])) {
            $param['order_type'] = 2;
        }
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        if (isset($param['third_order_id'])) {
            $param['order_id'] = $param['third_order_id'];
            unset($param['third_order_id']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/cancelorder' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
}
