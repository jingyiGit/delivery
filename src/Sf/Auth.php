<?php

namespace JyDelivery\Sf;

use JyDelivery\Kernel\Http;

/**
 * 授权
 */
trait Auth
{
    /**
     * 获取授权Url
     *
     * @param int $out_shop_id 第三方店铺ID
     * @return string
     */
    public function getAuthUrl($out_shop_id, $isPc = false)
    {
        $param = [
            'dev_id'      => $this->config['key'],
            'out_shop_id' => $out_shop_id,
        ];
        if ($isPc) {
            $url = $this->getHost() . '/artascope/cx/receipt/getpage/product/artascope/page/cbinding?';
        } else {
            $url = $this->getHost() . '/artascope/cx/receipt/getpage/product/artascope/page/storeBinding?';
        }
        return $url . http_build_query($param);
    }
    
    /**
     * 查询店铺账户余额
     *
     * @param array $param 参数
     * @return bool
     */
    public function getShopAccount($param)
    {
        // 1：顺丰店铺ID 2：接入方店铺ID
        if (!isset($param['shop_type'])) {
            $param['shop_type'] = 2;
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->getHost() . '/open/api/external/getshopaccountbalance' . $this->getSign($params), $params);
        if ($res['error_code'] == 0) {
            return $res['result'];
        }
        return $this->setError($res);
    }
}
