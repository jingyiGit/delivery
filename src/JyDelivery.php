<?php

namespace JyDelivery;

/**
 * Class Factory
 *
 * @method static \JyDelivery\Sf\Application                     Sf(array $config)
 * @method static \JyDelivery\ShanSong\Application               ShanSong(array $config)
 * @method static \JyDelivery\Dada\Application                   Dada(array $config)
 * @method static \JyDelivery\UU\Application                     UU(array $config)
 */
class JyDelivery
{
    /**
     * @param string $name
     * @param array  $config
     *
     */
    public static function make($name, $config = [])
    {
        $namespace   = self::studly($name);
        $application = "\\JyDelivery\\{$namespace}\\Application";
        return new $application($config);
    }
    
    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
    
    public static function studly($value)
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $value));
        return str_replace(' ', '', $value);
    }
}
